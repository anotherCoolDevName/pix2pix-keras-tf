import matplotlib.pyplot as plt
import numpy as np
import os

def inv_norm(X):
	return X*255.

def plot_generated(X_full, X_sketch, generator_model, epoch_num, batch_num):
	X_gen = generator_model.predict(X_sketch)

	X_sketch = inv_norm(X_sketch)[:8]
	X_gen = inv_norm(X_gen)[:8]
	X_full = inv_norm(X_full)[:8]

	X = np.concatenate((X_sketch,X_gen,X_full),axis=1)
	X = np.concatenate(X,axis=1)

	if not os.path.exists('./samples/'):
		os.makedirs('./samples/')

	plt.imsave('./samples/ep{}_bt{}.png'.format(epoch_num,batch_num),X)