import numpy as np

from keras.optimizers import Adam
from dcgan import *
import patch, logger
from batch import batch_generator, get_disc_batch, gen_batch

import time

import pdb

from keras.utils import generic_utils as keras_generic_utils

img_rows = 256
img_cols = 256
img_cnls = 3

patch_rows = 64
patch_cols = 64
#patch_rows = img_rows
#patch_cols = img_cols


n_patches, patch_gan_dim = patch.num_patches(img_rows = img_rows, img_cols = img_cols, img_cnls = img_cnls, patch_rows=patch_rows, patch_cols=patch_cols)
#pdb.set_trace()

generator_nn = generator_unet(img_rows, img_cols, img_cnls, img_cnls)
#generator_nn = generator_ae(img_rows, img_cols, img_cnls, img_cnls)
generator_nn.summary()

discriminator_nn = discriminator_patchgan(img_rows=img_rows, img_cols=img_cols, img_cnls=img_cnls, patch_rows=patch_rows, patch_cols = patch_cols, n_patches=n_patches)
#discriminator_nn = discriminator_dcgan(img_rows=img_rows, img_cols=img_cols, img_cnls=img_cnls)
discriminator_nn.summary()

discriminator_nn.trainable = False

dcgan_nn = DCGAN(generator_nn,discriminator_nn,img_rows,img_cols,img_cnls,patch_rows,patch_cols)
#dcgan_nn.summary()

opt_discriminator = Adam(lr=1E-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
opt_generator = Adam(lr=1E-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
opt_dcgan = Adam(lr=1E-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

generator_nn.compile(loss='mae', optimizer=opt_generator)

loss = ['mae', 'binary_crossentropy']
loss_weights = [1E2, 1]
dcgan_nn.compile(loss=loss, loss_weights=loss_weights, optimizer=opt_dcgan)

discriminator_nn.trainable = True
discriminator_nn.compile(loss='binary_crossentropy', optimizer=opt_discriminator)

batch_size = 1
data_path = './sketch-data'
n_epoch = 100
n_images_per_epoch = 400

print('Pray to Lord, All goes well...\n')
for epoch in range(n_epoch):
    print('Epoch {}'.format(epoch))
    batch_counter = 1
    start = time.time()
    progbar = keras_generic_utils.Progbar(n_images_per_epoch)

    tng_gen = batch_generator(batch_size=batch_size,img_rows=img_rows,img_cols=img_cols)

    for mini_batch_i in range(0,n_images_per_epoch,batch_size):
        print('Batch {}'.format(mini_batch_i))
        X_train_decoded_imgs, X_train_original_imgs = next(tng_gen)
        X_discriminator, y_discriminator = get_disc_batch(X_train_original_imgs,X_train_decoded_imgs, generator_nn, batch_counter, patch_rows = patch_rows, patch_cols = patch_cols)
        disc_loss = discriminator_nn.train_on_batch(X_discriminator, y_discriminator)

        X_gen_target, X_gen = next(gen_batch(X_train_original_imgs, X_train_decoded_imgs, batch_size))
        y_gen = np.zeros((X_gen.shape[0], 2), dtype=np.uint8)
        y_gen[:, 1] = 1

        discriminator_nn.trainable = False

        gen_loss = dcgan_nn.train_on_batch(X_gen,[X_gen_target,y_gen])

        discriminator_nn.trainable = True

        batch_counter +=1

        D_log_loss = disc_loss
        gen_total_loss = gen_loss[0].tolist()
        gen_total_loss = min(gen_total_loss,1000000)
        gen_mae = gen_loss[1].tolist()
        gen_mae = min(gen_mae,1000000)
        gen_log_loss = gen_loss[2].tolist()
        gen_log_loss = min(gen_log_loss,1000000)

        progbar.add(batch_size, values=[("Dis logloss", D_log_loss), ("Gen total", gen_total_loss), ("Gen L1 (mae)", gen_mae), ("Gen logloss", gen_log_loss)])
        
        if (batch_counter%2 == 0):
            logger.plot_generated(X_train_original_imgs,X_train_decoded_imgs, generator_nn, epoch, mini_batch_i)
            print('some images generated\n')












