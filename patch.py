import numpy as np

def num_patches(img_rows = 256, img_cols = 256, img_cnls = 3, patch_rows=64, patch_cols=64):
	n_patches = (img_rows//patch_rows)*(img_cols//patch_cols)	
	disc_dim = (patch_rows,patch_cols,img_cnls)
	return n_patches,disc_dim

def extract_patches(images, patch_rows=64, patch_cols=64):
	img_rows = np.shape(images)[1]
	img_cols = np.shape(images)[2]

	x_spots = range(0,img_cols, patch_cols)
	y_spots = range(0,img_rows, patch_rows)

	all_patches = []

	for y in y_spots:
		for x in x_spots:
			images_patches = images[:,y:y+patch_rows,x:x+patch_cols,:]
			all_patches.append(np.asarray(images_patches, dtype=np.float32))

	return all_patches

