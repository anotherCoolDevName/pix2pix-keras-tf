import numpy as np
#import glob, pickle
#import os, sys
#import cv2
from keras.models import Sequential, Model
from keras.layers import Flatten, Dense, Input
from keras.layers.merge import concatenate
from keras.layers import Reshape, Lambda
from keras.layers.core import Activation, Dropout
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import UpSampling2D
from keras.layers.convolutional import Convolution2D, MaxPooling2D, Deconvolution2D
from keras.layers.core import Flatten
#from PIL import Image
from keras import backend as K

from patch import extract_patches

import pdb

def discriminator_dcgan(img_rows=256, img_cols=256, img_cnls=3):
	model = Sequential()
	
	model.add(Convolution2D(64,(4,4),padding='same',input_shape=(img_rows, img_cols,img_cnls)))
	model.add(BatchNormalization())
	model.add(Activation('tanh'))
	model.add(MaxPooling2D(pool_size=(2,2)))

	model.add(Convolution2D(128,(4,4),padding='same'))
	model.add(BatchNormalization())
	model.add(Activation('tanh'))
	model.add(MaxPooling2D(pool_size=(2,2)))

	model.add(Convolution2D(512,(4,4),padding='same'))
	model.add(BatchNormalization())
	model.add(Activation('tanh'))

	model.add(Convolution2D(1,(4,4),padding='same'))
	model.add(BatchNormalization())
	model.add(Activation('sigmoid'))

	return model

def discriminator_patchgan(img_rows=256, img_cols=256, img_cnls=3, patch_rows=64, patch_cols = 64, n_patches=16):
	input_layer = Input(shape=(patch_rows,patch_cols,img_cnls))
	num_filters_start = 64
	n_conv = int(np.floor(np.log(img_rows)/np.log(2)))
	filters_list = [num_filters_start*min(8,(2**i))for i in range(n_conv)]

	disc_out = Convolution2D(64,(4,4),padding='same', strides=(2,2))(input_layer)
	disc_out = LeakyReLU(alpha=0.2)(disc_out)

	for i, filter_size in enumerate(filters_list[1:]):
		disc_out = Convolution2D(filter_size,(4,4),padding='same',strides=(2,2))(disc_out)
		disc_out = BatchNormalization()(disc_out)
		disc_out = LeakyReLU(alpha=0.2)(disc_out)

	patch_gan_disc = generate_patch_gan_loss(last_disc_conv_layer = disc_out, patch_rows = patch_rows, patch_cols = patch_cols, img_cnls = img_cnls, input_layer = input_layer, n_patches = n_patches)

	return patch_gan_disc

def generate_patch_gan_loss(last_disc_conv_layer, patch_rows, patch_cols, img_cnls, input_layer, n_patches):
	list_input = [Input(shape=(patch_rows,patch_cols, img_cnls))for i in range(n_patches)]
	x_flat = Flatten()(last_disc_conv_layer)
	x = Dense(2, activation='softmax')(x_flat)
	patch_gan = Model(outputs=[x,x_flat],inputs=[input_layer])

	x = [patch_gan(patch)[0] for patch in list_input]
	x_mbd = [patch_gan(patch)[1] for patch in list_input]

	if len(x)>1:
		x = concatenate(x)
	else:
		x = x[0]

	if len(x_mbd)>1:
		x_mbd = concatenate(x_mbd)
	else:
		x_mbd = x_mbd[0]

	n_kernels =100
	dim_per_kernel = 5

	M = Dense(n_kernels*dim_per_kernel,use_bias=False, activation = None)
	MBD = Lambda(minb_disc,output_shape = lambda_output)

	x_mbd = M(x_mbd)
	x_mbd = Reshape((n_kernels, dim_per_kernel))(x_mbd)
	x_mbd = MBD(x_mbd)
	x = concatenate([x,x_mbd])

	x_out = Dense(2, activation="softmax")(x)

	discriminator = Model( outputs = [x_out], inputs = list_input)
	return discriminator

def lambda_output(input_shape):
	return input_shape[:2]

def minb_disc(x):
	diffs = K.expand_dims(x,3) - K.expand_dims(K.permute_dimensions(x,[1,2,0]), 0)
	abs_diffs = K.sum(K.abs(diffs),2)
	x = K.sum(K.exp(-abs_diffs),2)

	return x

def generator_ae(img_rows, img_cols, img_cnls, num_output_channels):
	filter_sizes = [64,128,256,512,512,512,512,512]
	encoder = Input(shape=(img_rows,img_cols,img_cnls))

	for filter_size in filter_sizes:
		encoder = Convolution2D(filter_size,(4,4),padding='same',strides=(2,2))(encoder)
		if filter_size!=64:
			encoder = BatchNormalization()(encoder)
		encoder = LeakyReLU(alpha=0.2)(encoder)

	filter_sizes = [512,512,512,512,512,256,128,64]
	decoder = encoder
	for filter_size in filter_sizes:
		decoder = UpSampling2D(size=(2,2))(decoder)
		decoder = Convolution2D(filter_size,(4,4),padding="same")(decoder)
		decoder = BatchNormalization()(decoder)
		decoder = Dropout(rate=0.5)(decoder)
		decoder = Activation('relu')(decoder)

	decoder = Convolution2D(num_output_channels,(4,4),padding='same')(decoder)
	generator = Activation('tanh')(decoder)

	return generator

def generator_unet(img_rows, img_cols, img_cnls, num_output_channels):
	input_layer = Input(shape=(img_rows,img_cols,img_cnls))

	en_1 = Convolution2D(64,(4,4),padding='same',strides=(2,2))(input_layer)
	en_1 = LeakyReLU(alpha=0.2)(en_1)

	en_2 = Convolution2D(128,(4,4),padding='same',strides=(2,2))(en_1)
	en_2 = BatchNormalization()(en_2)
	en_2 = LeakyReLU(alpha=0.2)(en_2)

	en_3 = Convolution2D(256,(4,4),padding='same',strides=(2,2))(en_2)
	en_3 = BatchNormalization()(en_3)
	en_3 = LeakyReLU(alpha=0.2)(en_3)

	en_4 = Convolution2D(512,(4,4),padding='same',strides=(2,2))(en_3)
	en_4 = BatchNormalization()(en_4)
	en_4 = LeakyReLU(alpha=0.2)(en_4)

	en_5 = Convolution2D(512,(4,4),padding='same',strides=(2,2))(en_4)
	en_5 = BatchNormalization()(en_5)
	en_5 = LeakyReLU(alpha=0.2)(en_5)

	en_6 = Convolution2D(512,(4,4),padding='same',strides=(2,2))(en_5)
	en_6 = BatchNormalization()(en_6)
	en_6 = LeakyReLU(alpha=0.2)(en_6)

	en_7 = Convolution2D(512,(4,4),padding='same',strides=(2,2))(en_6)
	en_7 = BatchNormalization()(en_7)
	en_7 = LeakyReLU(alpha=0.2)(en_7)

	en_8 = Convolution2D(512,(4,4),padding='same',strides=(2,2))(en_7)
	en_8 = BatchNormalization()(en_8)
	en_8 = LeakyReLU(alpha=0.2)(en_8)

	de_1 = UpSampling2D(size=(2,2))(en_8)
	de_1 = Convolution2D(512,(4,4),padding='same')(de_1)
	de_1 = BatchNormalization()(de_1)
	de_1 = Dropout(rate=0.5)(de_1)
	de_1 = concatenate([de_1, en_7])
	de_1 = Activation('relu')(de_1)

	de_2 = UpSampling2D(size=(2,2))(de_1)
	de_2 = Convolution2D(512,(4,4),padding='same')(de_2)
	de_2 = BatchNormalization()(de_2)
	de_2 = Dropout(rate=0.5)(de_2)
	de_2 = concatenate([de_2, en_6])
	de_2 = Activation('relu')(de_2)

	de_3 = UpSampling2D(size=(2,2))(de_2)
	de_3 = Convolution2D(512,(4,4),padding='same')(de_3)
	de_3 = BatchNormalization()(de_3)
	de_3 = Dropout(rate=0.5)(de_3)
	de_3 = concatenate([de_3, en_5])
	de_3 = Activation('relu')(de_3)

	de_4 = UpSampling2D(size=(2,2))(de_3)
	de_4 = Convolution2D(512,(4,4),padding='same')(de_4)
	de_4 = BatchNormalization()(de_4)
	de_4 = Dropout(rate=0.5)(de_4)
	de_4 = concatenate([de_4, en_4])
	de_4 = Activation('relu')(de_4)

	de_5 = UpSampling2D(size=(2,2))(de_4)
	de_5 = Convolution2D(512,(4,4),padding='same')(de_5)
	de_5 = BatchNormalization()(de_5)
	de_5 = Dropout(rate=0.5)(de_5)
	de_5 = concatenate([de_5, en_3])
	de_5 = Activation('relu')(de_5)

	de_6 = UpSampling2D(size=(2,2))(de_5)
	de_6 = Convolution2D(512,(4,4),padding='same')(de_6)
	de_6 = BatchNormalization()(de_6)
	de_6 = Dropout(rate=0.5)(de_6)
	de_6 = concatenate([de_6, en_2])
	de_6 = Activation('relu')(de_6)

	de_7 = UpSampling2D(size=(2,2))(de_6)
	de_7 = Convolution2D(512,(4,4),padding='same')(de_7)
	de_7 = BatchNormalization()(de_7)
	de_7 = Dropout(rate=0.5)(de_7)
	de_7 = concatenate([de_7, en_1])
	de_7 = Activation('relu')(de_7)

	de_8 = UpSampling2D(size=(2,2))(de_7)
	de_8 = Convolution2D(num_output_channels,(4,4),padding='same')(de_8)
	de_8 = Activation('tanh')(de_8)

	unet_gen = Model( outputs=[de_8], inputs=[input_layer])
	return unet_gen

def DCGAN(generator_model,discriminator_model,img_rows, img_cols, img_cnls, patch_rows, patch_cols):
	generator_input = Input(shape=(img_rows,img_cols,img_cnls))
	generated_image = generator_model(generator_input)

	#generated_image_arr = K.eval(generated_image)

	#pdb.set_trace()
	#list_gen_patches = extract_patches(generated_image_arr,patch_rows=patch_rows,patch_cols=patch_cols)
	list_row_idx = [(i * patch_rows, (i + 1) * patch_rows) for i in range(int(img_rows / patch_rows))]
	list_col_idx = [(i * patch_cols, (i + 1) * patch_cols) for i in range(int(img_cols / patch_cols))]

	list_gen_patch = []
	for row_idx in list_row_idx:
		for col_idx in list_col_idx:
			x_patch = Lambda(lambda z: z[:, row_idx[0]:row_idx[1],
				col_idx[0]:col_idx[1],:], output_shape=(patch_rows,patch_cols,img_cnls))(generated_image)
			list_gen_patch.append(x_patch)


	dcgan_output = discriminator_model(list_gen_patch)
	#dcgan_output = discriminator_model(generated_image)

	dc_gan = Model(outputs=[generated_image,dcgan_output],inputs=[generator_input])
	return dc_gan





