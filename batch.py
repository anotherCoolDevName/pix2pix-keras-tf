import numpy as np
from patch import extract_patches
import os
import cv2

import pdb

def normalize(X):
	return X/255.0

def get_disc_batch(X_original_batch, X_decoded_batch, generator_model, batch_counter, patch_rows, patch_cols):
	if batch_counter%2 ==0:
		X_disc = generator_model.predict(X_decoded_batch)
		y_disc = np.zeros((X_disc.shape[0],2), dtype =np.uint8)
		y_disc[:,0] = 1
	else:
		X_disc = X_original_batch
		y_disc = np.zeros((X_disc.shape[0],2), dtype =np.uint8)
		y_disc[:,1] = 1

	X_disc = extract_patches(images=X_disc,patch_rows=patch_rows, patch_cols=patch_cols)

	return X_disc, y_disc

def gen_batch(X1,X2,batch_size):
	idx = np.random.choice(X1.shape[0],batch_size,replace=False)
	x1 = X1[idx]
	x2 = X2[idx]
	yield x1, x2

def batch_generator(batch_size=10, img_rows = 256, img_cols = 256,data_dir_name='./sketch-data'):
	while True:
		target_images = []
		edge_images = []
		flist = np.random.choice(os.listdir(data_dir_name+'/image'),batch_size,replace=False)
		#pdb.set_trace()
		for fname in flist:
			img_path = data_dir_name+'/image/'+fname
			edg_path = data_dir_name+'/edge/'+fname

			target_images.append(cv2.resize(cv2.imread(img_path),(img_rows,img_cols)))
			edge_images.append(cv2.resize(cv2.imread(edg_path),(img_rows,img_cols)))

		target_images = normalize(np.array(target_images,dtype=np.float32).reshape((batch_size,img_rows,img_cols,-1)))
		edge_images = normalize(np.array(edge_images,dtype=np.float32).reshape((batch_size,img_rows,img_cols,-1)))

		yield target_images, edge_images

